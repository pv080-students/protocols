# ---
# jupyter:
#   jupytext:
#     formats: py,ipynb
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.4
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# # Seminar 11: Alice's Point of View

# This notebook contains Alice's part of the implementation of the authentication protocol from the slides.

from pv080_crypto import send_message, recv_message, load_cert
from pv080_crypto import rsa_encrypt, aes_decrypt, aes_encrypt
import secrets

# ### Initialization

# Specify your own UČOs here.

alice_uco = 232886
bob_uco = 485305

# Alice trusts Bob's public key contained in the certificate stored in `bob-cert.pem`.

bob_public_key = load_cert("bob-cert.pem").public_key()

# ### The Protocol

# From here onwards, the cells contain individual steps of the protocol. The cells must be run in an interleaved manner with the ones in the notebook for Bob.
#
# If a message is incoming ($\mathtt{A \leftarrow B}$), the corresponding cell in the other notebook must be executed first.
#
# **Do not make any changes in these cells!**
#
# Cells include debug output for your convenience.

# $1. \ \mathtt{A \rightarrow B: A, Pu_B(K_{AB}), K_{AB}(K_{MAC})}$

# +
symmetric_key = secrets.token_bytes(32) # K_AB
encrypted_symmetric_key = rsa_encrypt(bob_public_key, symmetric_key) # Pu_B(K_AB)
mac_key = secrets.token_bytes(32) # K_MAC
encrypted_mac_key = aes_encrypt(symmetric_key, mac_key) # K_AB(K_MAC)
sent_identity1 = b"A"
payload = sent_identity1 + encrypted_symmetric_key + encrypted_mac_key
send_message(alice_uco, bob_uco, payload)

# Debug output included for your convenience:
print("DEBUG OUTPUT:")
print(f"Generated symmetric_key: {symmetric_key.hex()}")
print(f"Encrypted symmetric key: {encrypted_symmetric_key.hex()}")
print(f"Generated MAC key: {mac_key.hex()}")
print(f"Encrypted MAC key: {encrypted_mac_key.hex()}")
print(f"Identity of this party: {sent_identity1}, in hex: {sent_identity1.hex()}")
print(f"Sent message: {payload.hex()}")
# -

# $2. \ \mathtt{A \leftarrow B: B, N_B, K_{AB}(N_B), K_{AB}(N_A)}$

# +
payload = recv_message(alice_uco)[bob_uco]
recv_identity, nonce_b, encrypted_nonce_b, encrypted_nonce_a = payload[:1], payload[1:1+32], \
    payload[1+32:1+32+64], payload[1+32+64:]
nonce_a = aes_decrypt(symmetric_key, encrypted_nonce_a)
decrypted_nonce_b = aes_decrypt(symmetric_key, encrypted_nonce_b)

# Authentication of other party:
print("Authentication of other party:")
if decrypted_nonce_b == nonce_b:
    print(f"SUCCESS: Other party {recv_identity} has successfully authenticated itself.")
else:
    print(f"FAILURE: Authentication failed. Decrypted nonce does not match received plaintext nonce.")
print()

# Debug output included for your convenience:
print("DEBUG OUTPUT:")
print(f"Received message: {payload.hex()}")
print(f"Identity of other party: {recv_identity}, in hex: {recv_identity.hex()}")
print(f"Received plaintext nonce B: {nonce_b.hex()}")
print(f"Received encrypted nonce B: {encrypted_nonce_b.hex()}")
print(f"Encrypted nonce B after decryption: {decrypted_nonce_b.hex()}")
print(f"Received encrypted nonce A: {encrypted_nonce_a.hex()}")
print(f"Encrypted nonce A after decryption: {nonce_a.hex()}")
# -

# $3. \ \mathtt{A \rightarrow B: A, N_A}$

# +
sent_identity2 = b"A"
payload = sent_identity2 + nonce_a
send_message(alice_uco, bob_uco, payload)

# Debug output included for your convenience:
print("DEBUG OUTPUT:")
print(f"Identity of this party: {sent_identity2}, in hex {sent_identity2.hex()}")
print(f"Decrypted nonce A: {nonce_a.hex()}")
print(f"Sent message: {payload.hex()}")
# -

