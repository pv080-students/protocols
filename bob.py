# ---
# jupyter:
#   jupytext:
#     formats: py,ipynb
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.4
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# # Seminar 11: Bob's Point of View
# This notebook contains Bob's part of the implementation of the authentication protocol from the slides.

from pv080_crypto import send_message, recv_message, load_private_key
from pv080_crypto import rsa_decrypt, aes_decrypt, aes_encrypt
import secrets

# ### Initialization

# Specify your own UČOs here.

alice_uco = 232886
bob_uco = 485305

# Bob will need to use his own private key which is stored in `bob-private-key.pem`.

bob_private_key = load_private_key("bob-private-key.pem")

# ### The Protocol

# From here onwards, the cells contain individual steps of the protocol. The cells must be run in an interleaved manner with the ones in the notebook for Alice.
#
# If a message is incoming ($\mathtt{B \leftarrow A}$), the corresponding cell in the other notebook must be executed first.
#
# **Do not make any changes in these cells!**
#
# Cells include debug output for your convenience.

# $1. \ \mathtt{B \leftarrow A: A, Pu_B(K_{AB}), K_{AB}(K_{MAC})}$

# +
payload = recv_message(bob_uco)[alice_uco]
recv_identity1, encrypted_symmetric_key, encrypted_mac_key = payload[:1], payload[1:1+256], payload[1+256:]
symmetric_key = rsa_decrypt(bob_private_key, encrypted_symmetric_key)
mac_key = aes_decrypt(symmetric_key, encrypted_mac_key)

# Debug output included for your convenience:
print("DEBUG OUTPUT:")
print(f"Received message: {payload.hex()}")
print(f"Identity of other party: {recv_identity1}, in hex: {recv_identity1.hex()}")
print(f"Received encrypted symmetric key: {encrypted_symmetric_key.hex()}")
print(f"Decrypted symmetric key: {symmetric_key.hex()}")
print(f"Received encrypted MAC key: {encrypted_mac_key.hex()}")
print(f"Decrypted MAC key: {mac_key.hex()}")
# -

# $2. \ \mathtt{B \rightarrow A: B, N_B, K_{AB}(N_B), K_{AB}(N_A)}$

# +
nonce_b = secrets.token_bytes(32) # N_B
nonce_a = secrets.token_bytes(32) # N_A
encrypted_nonce_b = aes_encrypt(symmetric_key, nonce_b) # K_AB(N_B)
encrypted_nonce_a = aes_encrypt(symmetric_key, nonce_a) # K_AB(N_A)
sent_identity = b"B"
payload = sent_identity + nonce_b + encrypted_nonce_b + encrypted_nonce_a
send_message(bob_uco, alice_uco, payload)

# Debug output included for your convenience:
print("DEBUG OUTPUT:")
print(f"Generated nonce B: {nonce_b.hex()}")
print(f"Encrypted nonce B: {encrypted_nonce_b.hex()}")
print(f"Generated nonce A: {nonce_a.hex()}")
print(f"Encrypted nonce A: {encrypted_nonce_a.hex()}")
print(f"Identity of this party: {sent_identity}, in hex: {sent_identity.hex()}")
print(f"Sent message: {payload.hex()}")
# -

# $3. \ \mathtt{B \leftarrow A: A, N_A}$

# +
payload = recv_message(bob_uco)[alice_uco]
recv_identity2, received_nonce_a = payload[:1], payload[1:]

# Protocol evaluation:
print("Authentication of other party:")
if recv_identity1 != recv_identity2:
    print(f"FAILURE: Authentication failed. Identity received in third message {recv_identity2} does not match "
          f"identity received in first message {recv_identity1}.")
elif received_nonce_a != nonce_a:
    print(f"FAILURE: Authentication failed. Received nonce does not match sent nonce.")
else:
    print(f"SUCCESS: Other party {recv_identity2} has successfully authenticated itself.")
print()

# Debug output included for your convenience:
print("DEBUG OUTPUT:")
print(f"Received message: {payload.hex()}")
print(f"Identity of other party: {recv_identity2}, in hex {recv_identity2.hex()}")
print(f"Received nonce A: {received_nonce_a.hex()}")
print(f"Sent nonce A: {nonce_a.hex()}")
# -
