# ---
# jupyter:
#   jupytext:
#     formats: py,ipynb
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.4
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# # Seminar 11: Attacker's Point of View
# In this notebook, you should attempt to intercept the communication between Alice and Bob and perform some attack on the protocol that is taking place between them.

from pv080_crypto import send_message, recv_message
from pv080_crypto import rsa_decrypt, rsa_encrypt, aes_decrypt, aes_encrypt
import secrets

# ### Initialization

# Specify the UČOs of Alice and Bob here.

alice_uco = 232886
bob_uco = 485305

# **Do not load the private key of Bob!** You should pretend that only Bob knows it. 

# ### The Attack
# The following cells are prepared for you to simplify your attack.
#
# Remember, when you want to replace a specific message from Alice to Bob ($\mathtt{A \rightarrow B}$), you should:
# 1. Wait till Alice sends the message. 
# 2. Receive the message as if you were Bob: `recv_message(bob_uco)[alice_uco]`
# 3. Send a modified message to Bob: `send_message(alice_uco, bob_uco, msg)`

# $1. \ \mathtt{A \rightarrow B: A, Pu_B(K_{AB}), K_{AB}(K_{MAC})}$

payload = recv_message(bob_uco)[alice_uco]
identity, encrypted_symmetric_key, encrypted_mac_key = payload[:1], payload[1:1+256], payload[1+256:]
# You can modify the parts of the message here
modified_payload = identity + encrypted_symmetric_key + encrypted_mac_key
send_message(alice_uco, bob_uco, modified_payload)

# $2. \ \mathtt{B \rightarrow A: B, N_B, K_{AB}(N_B), K_{AB}(N_A)}$

payload = recv_message(alice_uco)[bob_uco]
identity, nonce_b, encrypted_nonce_b, encrypted_nonce_a = payload[:1], payload[1:1+32], payload[1+32:1+32+64], payload[1+32+64:]
# You can modify the parts of the message here
modified_payload = identity + nonce_b + encrypted_nonce_b + encrypted_nonce_a
send_message(bob_uco, alice_uco, modified_payload)

# $3. \ \mathtt{A \rightarrow B: A, N_A}$

payload = recv_message(bob_uco)[alice_uco]
identity, received_nonce_a = payload[:1], payload[1:]
# You can modify the parts of the message here
modified_payload = identity + received_nonce_a
send_message(alice_uco, bob_uco, modified_payload)
